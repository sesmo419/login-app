package com.securedinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuredInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuredInventoryApplication.class, args);
	}

}

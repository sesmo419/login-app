package com.securedinventory.service;

import com.securedinventory.user.User;

public interface UserService {
	
	 public User findUserByEmail(String email);
	 
	 public void saveUser(User user);

}

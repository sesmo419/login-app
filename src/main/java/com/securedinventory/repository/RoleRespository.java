package com.securedinventory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.securedinventory.user.Role;

@Repository
public interface RoleRespository extends JpaRepository<Role, Integer> {
	Role findByRole(String role);

}
